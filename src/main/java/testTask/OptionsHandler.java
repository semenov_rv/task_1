package testTask;

import org.apache.commons.cli.*;

public class OptionsHandler {
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = null;
    ValidationData validationData = new ValidationData();

    public void initOptions(String[] args) {
        Options options = new Options();
        OptionsContainer[] values = OptionsContainer.values();

        for (OptionsContainer optionsContainer : values) {
            Option option = new Option(optionsContainer.getName(), optionsContainer.getLongName(), optionsContainer.isHasArg(), optionsContainer.getDescription());
            option.setRequired(optionsContainer.isSetRequired());
            options.addOption(option);
        }

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("Ошибка. Вы ввели не корректные параметры для команд.");
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("Опции : ", options);
            System.exit(1);
        }
    }
    public String getInputFile() throws ValidateException {
        String inputFile = null;
        if (cmd.hasOption("i")) {
            inputFile = cmd.getOptionValue("i");
        } else if (cmd.hasOption("inputFile")) {
            inputFile = cmd.getOptionValue("inputFile");
        }
        validationData.validateInputFile(inputFile);
        return inputFile;
    }

    public String getOutputFile() throws ValidateException {
        String outputFile = null;
        if (cmd.hasOption("o")) {
            outputFile = cmd.getOptionValue("o");
        } else if (cmd.hasOption("outputFile")) {
            outputFile = cmd.getOptionValue("outputFile");
        }
        validationData.validateOutputFile(outputFile);
        return outputFile;
    }

    public String getMyCharset() throws ValidateException {
        String myCharset = null;
        if (cmd.hasOption("c")) {
            myCharset = cmd.getOptionValue("c");
        } else if (cmd.hasOption("charset")) {
            myCharset = cmd.getOptionValue("charset");
        }
        validationData.validateCharset(myCharset);
        return myCharset;
    }
}

