package testTask;

public enum OptionsContainer {
    INPUT_FILE("i", "inputFile", true, "Имя файла, которое необходимо прочитать", true),
    OUTPUT_FILE("o", "outputFile", true, "Имя файла, куда программа запишет результат.", true),
    CHARSET("c", "charset", true, "Кодировка для данного файла", true);

    private String name;
    private String longName;
    private boolean hasArg;
    private String description;
    private boolean setRequired;

    OptionsContainer(String name, String longName, boolean hasArg, String description, boolean setRequired) {
        this.name = name;
        this.longName = longName;
        this.hasArg = hasArg;
        this.description = description;
        this.setRequired = setRequired;
    }
    public String getName() {
        return name;
    }

    public String getLongName() {
        return longName;
    }

    public boolean isHasArg() {
        return hasArg;
    }

    public String getDescription() {
        return description;
    }

    public boolean isSetRequired() {
        return setRequired;
    }
}
