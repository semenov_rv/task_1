package testTask;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class DataProcess {
    private AtomicLong counterAtomicLong = new AtomicLong();

    public Map<Character, Long> getSymbolsFromInputFile(Stream<String> stream) {
        return stream
                .flatMapToInt(String::chars)
                .mapToObj(symbol -> (char) symbol)
                .filter(character -> !Character.isSpaceChar(character))
                .peek(character -> counterAtomicLong.incrementAndGet())
                .collect(Collectors.groupingBy(symbol -> symbol, Collectors.counting()));
    }

    public List<String> getSortedStrings(Map<Character, Long> map) {
        BigDecimal counter = BigDecimal.valueOf(counterAtomicLong.get());
        System.out.println("Количество символов в файле: " + counter);

        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .map(getFormattedStringFromMap(counter))
                .collect(Collectors.toList());
    }

    private Function<Map.Entry<Character, Long>, String> getFormattedStringFromMap(BigDecimal counter) {
        return out -> out.getKey() + " : " + out.getValue()
                + " : " + getPercent(counter, out) + "%";
    }

    private BigDecimal getPercent(BigDecimal counter, Map.Entry<Character, Long> out) {
        final BigDecimal PERCENT = BigDecimal.valueOf(100);

        return PERCENT
                .divide(counter, 4, RoundingMode.HALF_EVEN)
                .multiply(BigDecimal.valueOf(out.getValue()));
    }
}




