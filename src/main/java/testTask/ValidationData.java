package testTask;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ValidationData {

    public void validateCharset(String charset) throws ValidateException {
        if (charset == null) {
            throw new ValidateException("Вы не ввели кодировку.");
        }
        try {
            Charset myCharset = Charset.forName(charset);
            if (!myCharset.isRegistered()) {
                throw new ValidateException("Кодировка не распознана!");
            }
        } catch (IllegalCharsetNameException | UnsupportedCharsetException exception) {
            throw new ValidateException("Кодировка не поддерживается.");
        }
    }

    public void validateOutputFile(String output) throws ValidateException {
        if (output == null) {
            throw new ValidateException("Вы не ввели конечное имя файла.");
        }
        if (output.startsWith(" ")) {
            throw new ValidateException("Имя файла не может начинаться с пробела.");
        }
    }

    public void validateInputFile(String input) throws ValidateException {
        Path path = Paths.get(input);
        if (input == null) {
            throw new ValidateException("Вы не ввели имя файла, который нужно прочитать.");
        }
        if (input.startsWith(" ")) {
            throw new ValidateException("Имя файла не может начинаться с пробела.");
        }
        if (Files.isDirectory(path)) {
            throw new ValidateException("Указанный файл - является директорией!");
        }
        if (input.isEmpty()) {
            throw new ValidateException("Вы ввели пустое имя файла. Введите верное имя файла.");
        }
        if (Files.notExists(path)) {
            throw new ValidateException("Входной файл не найден. Попробуйте еще раз!");
        } else {
            System.out.println("Входной файл найден и готов к работе: " + path);
        }
        if (!Files.isReadable(path)) {
            throw new ValidateException("Ошибка! Файл не читается!");
        }
    }
}

