package testTask;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;


public class Main {

    public static void main(String[] args) {
        System.out.println("\n*********************************");
        System.out.println("\nПрограмма начала свою работу. \n");
        System.out.println("*********************************\n");
        try {
            DataProcess dataProcess = new DataProcess();
            OptionsHandler optionsHandler = new OptionsHandler();

            optionsHandler.initOptions(args);
            Path inputFile = Paths.get(optionsHandler.getInputFile()) ;
            Path outputFile = Paths.get(optionsHandler.getOutputFile());
            Charset myCharset = Charset.forName(optionsHandler.getMyCharset());

            Map<Character, Long> symbolsFromInputFile = dataProcess.getSymbolsFromInputFile(Files.lines(inputFile));
            List<String> resultList = dataProcess.getSortedStrings(symbolsFromInputFile);
            writeToFile(outputFile, resultList, myCharset);

            System.out.println("\n*********************************");
            System.out.println("\nПрограмма завершила свою работу!\n");
            System.out.println("*********************************\n");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    public static void writeToFile(Path output, List<String> list, Charset charset) {
        try {
            Files.deleteIfExists(output);
            Files.write(output, list, charset);
            System.out.println("Данные успешно записаны в файл " + output + ".");
        } catch (IOException e) {
            System.out.println("Запись данных в файл не удалась!");
        }
    }
}





