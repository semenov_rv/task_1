package testTask;

public class ValidateException extends Exception {
    public ValidateException(String message) {
        super(message);
    }

}
