package testTask;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


class DataProcessTest {

    @Test
    void writeToFile(@TempDir Path temp) throws IOException {

        Path actualResult = temp.resolve("ActualResult.txt");
        List<String> expectedData = Stream.of("One", "Two", "Three").collect(Collectors.toList());

        Main.writeToFile(actualResult, expectedData, StandardCharsets.UTF_8);
        List<String> actual = Files.lines(actualResult).collect(Collectors.toList());

        assertLinesMatch(expectedData, actual);

    }
}